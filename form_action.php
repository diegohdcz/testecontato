<?php

use PHPMailer\PHPMailer\PHPMailer;


require __DIR__ . '/vendor/autoload.php';


$error = [];
$errorMessage = '';
$name = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_SPECIAL_CHARS) ;
$lastname=filter_input(INPUT_POST,'lastname', FILTER_SANITIZE_SPECIAL_CHARS);
$email = filter_input(INPUT_POST,'email',FILTER_VALIDATE_EMAIL);
$phone = filter_input(INPUT_POST,'phone',FILTER_VALIDATE_INT);
$textarea = filter_input(INPUT_POST,'textarea', FILTER_SANITIZE_SPECIAL_CHARS);

if(!empty($_POST)) {
    $name = $_POST['name'];
    $lastname = $_POST['lastname'];
    $email = $_POST['email'];
    $phone = $_POST['phone'];
    $textarea = $_POST['textarea'];

    if(empty($name)) {
        $error[] = "Preencha o campo Nome";
    }
    if(empty($lastname)) {
        $error[] = "Preencha o campo Sobrenome";
    }
    if(empty($email)) {
        $error[] = "Preencha o campo E-mail";
    }
    if(empty($phone)) {
        $error[] = "Preencha o campo Telephone";
    }
    if(empty($name)) {
        $error[] = "Preencha o campo Digite Sua Mensagem";
    }
    if(!empty($error)) {
        $allerrors = join('<br/>', $error);
        $errorMessage = "<p style='color: red;'>{$allErrors}</p>";
    }
}else{
    $mail = new PHPMailer();

    $mail->isSMTP();
    $mail->Host = 'smtp.TESTE';
    $mail->SMTPAuth = true;
    $mail->Username = 'SEU USUARIO';
    $mail->Password = 'SUA SENHA';
    $mail->SMTPSecure = 'tls';
    $mail->Port = 2525;
    $mail->setFrom($email, 'Mailtrap Website');
        $mail->addAddress('SEU EMAIL', 'Me');
        $mail->Subject = 'Novo contato do twochat';

        
        $mail->isHTML(true);

        $bodyParagraphs = ["Nome: {$name}","Sobrenome:{$lastname}", "Email: $email","Telephone:"[$phone], "Mensagem:", nl2br($textarea)];
        $body = join('<br />', $bodyParagraphs);
        $mail->Body = $body;

        echo $body;
        if($mail->send()){

            header('Location: index.html');
        } else {
            $errorMessage = 'Aconteceu um errro com a sessão ' . $mail->ErrorInfo;
        }
    }
